const items = require('./data');

//Get all items that are available 
function availableItems(items){
    let result = items.filter((item) =>{
        return item.available === true
    })
    return result
}


//Get all items containing only Vitamin C.
function searchVitamin(items, vitamin){
    let result = items.filter((item) =>{
      return item.contains==vitamin
    })
     return result
}


// Get all items containing Vitamin A.
function seachVitamins(items, vitamin){
    let result = items.filter((item)=>{
        if(item.contains.includes(vitamin)){
            return item
        }
    })
    return result
}


// items based on vitamins
function itemBasedOnVitamins(items) {
    let result = {};
    items.map((item) =>{
       let vitamins = item.contains.split(', ');
       vitamins.map((vitamin)=>{
        result[vitamin]
            ? result[vitamin] = [...result[vitamin],item.name]
            : result[vitamin]=[item.name]
       })
    })
    return result
    
}


// Sort items based on number of Vitamins they contain
function sortNumberOfVitamins(items){
    return items.sort((fisrt, second) =>{
        return fisrt.contains.length - second.contains.length
    }).reverse();
}